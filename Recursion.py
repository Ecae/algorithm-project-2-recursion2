__author__ = 'jhoeflich2017'


n = input('Enter a number ')

def sumDigits(n):
    if n == 0:
        return 0
    x = list(str(n))
    z = len(x)
    q = 0
    y = 0
    while q < z:
        z -= 1
        y += int(x[z])
    return y

print('Sum of digits = ' + str(sumDigits(n)))
